const User = require("../models/User.js");
const Product = require("../models/Product.js");
const auth = require("../auth.js")

// product registration

module.exports.addProduct = (reqBody, admin) => {

	if(admin.isAdmin){

		let newProduct = new Product({
				productName : reqBody.productName,
				productBrand: reqBody.productBrand,
				description : reqBody.description,
				price : reqBody.price
			});

			
			return newProduct.save().then((product, error) => {
				
				if (error) {
					return false;
				} else {
					return true;
				};

			});

	} else {
		return ("Authorized Personnel Only")
	}

	

};


// Get all Products

module.exports.getAllProduct = () => {
	return Product.find({isActive: true}).then(result => {
		result.orders= [];
		return result;
	});
}


// Update Product

module.exports.updateProduct = (reqParams, reqBody, admin) => {

	if(admin.isAdmin){

		console.log(reqParams);
		let updateProduct = {
			productName : reqBody.productName,
			productBrand: reqBody.productBrand,
			description: reqBody.description,
			price: reqBody.price
		};

		return	Product.findByIdAndUpdate(reqParams.productId, updateProduct).then((product, error) =>{
			
			if(error){
				return false;

		
			} else {
				return true
			}
		})

	} else {
		return ("Authorized Personnel Only");
	}

}
// 

module.exports.retrieveOneProduct = (reqParams) => {

	return Product.findById(reqParams.productId)


}

// Archive Product

module.exports.archiveProducts = (reqParams, data) => {
	
	if (data.isAdmin) {
		let archiveProd = {
			isActive: false
		};
		
		return Product.findByIdAndUpdate(reqParams.productId, archiveProd).then((product, error) =>{

			if(error){
				return false;

			
			} else {
				return true
			}
		})
	}else{
		return ("Authorized Personnel Only");
	}
	

}
