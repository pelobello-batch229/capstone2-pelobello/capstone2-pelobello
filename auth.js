const jwt = require("jsonwebtoken");


const secret = "Product4Sale";

// Token Creation
module.exports.createAccessToken = (user) => {
	const data = {
		id: user._id,
		email: user.email,
		isAdmin: user.isAdmin
	}

	return jwt.sign(data, secret, {} )
}

// Verifying The token if its legit and tampered
module.exports.verify = (req ,res, next) => {
	let token = req.headers.authorization


	if(typeof token !== "undefined"){
	
		token = token.slice(7, token.length);

		return jwt.verify(token,secret, (err, data) =>{

			if(err){
				return res.send({auth: "Token is Invalid"})
			} else {
				next();
			}

		})	
	} else {
		return res.send({auth: "Token Undefined"})
	}
}

// Token Decryption  gain access to products

module.exports.decode = (token) =>{

	if(typeof token !== "undefined"){

		token = token.slice(7, token.length);

		return jwt.verify(token, secret, (err, data) =>{

			if(err){

				return null;

			} else {
				return jwt.decode(token, {complete: true}).payload;
			}
		})

	}else{
		return null;
	}
}

