const express = require("express");
const router = express.Router();
const productControllers = require("../controllers/productControllers.js");
const auth = require("../auth");

// add prodcuts //  admin user only
router.post("/add", auth.verify, (req,res) =>{

     const admin = {
        isAdmin: auth.decode(req.headers.authorization).isAdmin
    }

	productControllers.addProduct(req.body , admin).then(controllerResult => res.send(controllerResult));
})


// get all products
router.get("/all", (req, res) => {
    productControllers.getAllProduct().then(controllerResult => {
        res.send(controllerResult)
    });
})

// get Single products
router.get("/:productId", auth.verify, (req, res) => {

    productControllers.retrieveOneProduct(req.params).then(controllerResult => {
        res.send(controllerResult)
    });
})

// update product by id // admin user only
router.put("/:productId", auth.verify, (req,res) => {

        const admin = {
          isAdmin: auth.decode(req.headers.authorization).isAdmin
        } 

    productControllers.updateProduct(req.params, req.body, admin).then(controllerResult => res.send(controllerResult));
})


//Archive Product admin user only
router.put("/:productId/archive", auth.verify, (req,res) => {

      const admin = {
        isAdmin: auth.decode(req.headers.authorization).isAdmin
    }
    productControllers.archiveProducts(req.params, admin).then(resultFromController => res.send(resultFromController));
})

module.exports = router;